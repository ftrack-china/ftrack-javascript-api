'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
// :copyright: Copyright (c) 2016 ftrack
/**
 * Constant namespace
 * @namespace constant
 */

var SERVER_LOCATION_ID = '3a372bde-05bc-11e4-8908-20c9d081909b';

exports.default = {
  SERVER_LOCATION_ID: SERVER_LOCATION_ID
};
module.exports = exports['default'];